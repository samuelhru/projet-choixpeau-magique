# -*- coding: utf-8 -*-
"""
Description du programme :
Auteur : NOURRY Nathan, HURTADO Samuel, LAUNAY Adam.
Licence : CC BY-NC-SA 3.0
"""
import csv
from math import sqrt

profile_1 = {'Courage': '9', 'Ambition': '2', 'Intelligence': '8', 'Good': '9'}
profile_2 = {'Courage': '6', 'Ambition': '7', 'Intelligence': '9', 'Good': '7'}
profile_3 = {'Courage': '3', 'Ambition': '8', 'Intelligence': '6', 'Good': '3'}
profile_4 = {'Courage': '2', 'Ambition': '3', 'Intelligence': '7', 'Good': '8'}
profile_5 = {'Courage': '3', 'Ambition': '4', 'Intelligence': '8', 'Good': '8'}

#  profile_1, profile_2, profile_3, profile_4, profile_5 #sample


def fusion(tab_2, tab_1):
    '''
    Fusion de deux tables
    Entree: Deux tables, (list)
    Sortie: Un tableau, (list)
    '''
    for char in tab_2:
        for stats in tab_1:
            if char['Name'] == stats['Name']:
                char['House'] = stats['House']
    return tab_2


def sous(stat, stat_2):
    '''
    Calcule le carré de la difference entre deux caracteristiques
    Entree: Caracteristiques, (int)
    Sortie: la difference, (int)
    '''
    dist = (stat - stat_2) ** 2
    return dist


def length(tab: list, profile):
    '''
    Calcule la distance euclidienne pour chaque personnage
    Entree: table et le profil type, (list, dict)
    Sortie: table, (dict)
    '''
    for stud in tab:
        dist_1 = sqrt(sum([sous(stud[stat], profile[stat])
                      for stat in profile_1.keys()]))
        stud['Distance'] = dist_1
    return tab


def house_association(tab, i):
    '''
    Cherche la maison la plus présente à la quelle le personnage sera envoyé
    Entree: table et le nombre de K plus proche voisin, (list, int)
    Sortie: la maison, (str), les k plus proches voisins
    '''
    houses = [{'maison du profil': 'Gryffindor', 'apearence': 0},
              {'maison du profil': 'Hufflepuff', 'apearence': 0},
              {'maison du profil': 'Ravenclaw', 'apearence': 0},
              {'maison du profil': 'Slytherin', 'apearence': 0}]
    for house_1 in tab[:i]:
        for n in houses:
            if house_1['House'] == n['maison du profil']:
                n['apearence'] += 1
    plus_p_v = tab[:i]
    return max(houses, key=lambda k: k['apearence']), plus_p_v


def int_tryparse(string):
    '''
    essaye de renvoyer un string en int sinon le revois comme tel
    entree: sting
    sortie: int
    '''
    try:
        return int(string)
    except ValueError:
        return string

with open("Characters.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    chars = [{Key: value.replace(
        '\xa0', ' ') for Key, value in element.items()} for element in reader]

with open("Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    perso_chars = [{key: int_tryparse(value.replace(
        '\xa0', ' ')) for key, value in element.items()} for element in reader]

for p in profile_1, profile_2, profile_3, profile_4, profile_5:
    for k in p:
        p[k] = int(p[k])


def IHM():

    print("La maison du profile 1 et ses plus proches voisins sont :\n",
    house_association(sorted(length(fusion(perso_chars, chars), profile_1),
                             key=lambda x: x['Distance']), 5), "\n")
    print("La maison du profile 2 et ses plus proches voisins sont :\n",
    house_association(sorted(length(fusion(perso_chars, chars), profile_2),
                             key=lambda x: x['Distance']), 5), "\n")
    print("La maison du profile 3 et ses plus proches voisins sont :\n",
    house_association(sorted(length(fusion(perso_chars, chars), profile_3),
                             key=lambda x: x['Distance']), 5), "\n")
    print("La maison du profile 4 et ses plus proches voisins sont :\n",
    house_association(sorted(length(fusion(perso_chars, chars), profile_4),
                             key=lambda x: x['Distance']), 5), "\n")
    print("La maison du profile 5 et ses plus proches voisins sont :\n",
    house_association(sorted(length(fusion(perso_chars, chars), profile_5),
                             key=lambda x: x['Distance']), 5), "\n")

     
print(IHM())

'''
FONCTION  house_association(tab, i)
   VARIABLE
     i <- K plus proches voisins
     tab <- tableau trié de la fusion de dictionnaires auquels la distance est
            ajouté avec le profile type
   DEBUT
     POUR house_1 DANS tab[:i]
         POUR n DANS houses
           SI house_1['house'] == n['house']
              n['number'] += 1
     RENVOYER max(houses, key <- lambda k:k['number']), tab[:i]
   FIN
'''
